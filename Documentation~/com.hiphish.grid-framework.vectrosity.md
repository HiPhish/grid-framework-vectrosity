# Vectrosity support for Grid Framework

Vectrosity is a 3rd party Unity plugin for rendering 3D vector lines and Grid
Framework offers support for it by allowing you to access the points of a
renderer in a way that is fit for Vectrosity.


## Package contents

- `Runtime/Extensions/Points.cs`: Extension methods for grid renderers which
	compute coordinates for use with Vectrosity.
- `Runtime/Rendering/Backends/VectrosityBackend.cs`: A new rendering backed
	component, attach it to your grid objects.


## Installation instructions

Please refer to the official packages documentation on how to add a custom
package. I recommend adding this Git repository as a dependency to your project
and specifying the release tag you wish to use:

```json
{
  "dependencies": {
    "com.hiphish.grid-framework.vectrosity": "https://gitlab.com/hiphish/grid-framework-3-vectrosity.git#v3.0.0"
  }
}
```

You will need Grid Framework and Vectrosity installed for the code to compile
correctly.


## Requirements

- [Grid Framework](http://hiphish.github.io/grid-framework/)
- [Vectrosity](https://starscenesoftware.com/vectrosity.html)


## Getting started

Let us render a grid using Vectrosity. Set up a grid for rendering as usual,
try it out with one of the standard renderers first to make sure the scene is
set up correctly (see the Grid Framework user manual). Once everything is set
up you can remove the backend again.

1. Add the backend component (Components -> Grid Framework -> Rendering ->
	 Vectrosity Backend)
2. Set the properties for the resulting vector line; you can use the assets
	 that come with the included samples
3. Run your scene

You can have a look at the included samples to see the code in action. All
included classes include documentation comments which your editor tooling
should be able to detect.


## Samples

- Basic: A static scene with dynamic grids. The resizing grid changes its
	properties at runtime. The colour-swapping grid generates a vector line
	directly instead of using the included backend.
