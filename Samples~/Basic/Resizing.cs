﻿using UnityEngine;
using GridFramework.Grids;

namespace GridFramework.Examples.Vectrosity {
	public class Resizing : MonoBehaviour {
		private HexGrid _grid;
		private bool isGrowing = true;

		void Awake() {
			_grid = GetComponent<HexGrid>();
		}

		void Update() {
			ResizeGrid(_grid);
		}

		private void ResizeGrid(HexGrid grid){
			var current  = grid.Radius;
			var target   = isGrowing ? 3f : 2f;
			var maxDelta = .5f * Time.deltaTime;
			var atLimit  = Mathf.Abs(grid.Radius - target) <= Mathf.Epsilon;

			grid.Radius = Mathf.MoveTowards(current , target, maxDelta);

			if (isGrowing) {
				isGrowing &= !atLimit;
			} else {
				isGrowing |= atLimit;
			}
		}
	}
}
