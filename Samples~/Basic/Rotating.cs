﻿using UnityEngine;

namespace GridFramework.Examples.Vectrosity {
	/// <summary> A simple script which makes the object rotate around its origin. </summary>
	public class Rotating : MonoBehaviour {
		void Update() {
			var dt = Time.deltaTime;

			var rotation1 = 10 * Vector3.right * dt;
			var rotation2 = 15 * Vector3.up    * dt;
			transform.Rotate(rotation1);
			transform.Rotate(rotation2, Space.World);
		}
	}
}
