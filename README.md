# Vectrosity support of Grid Framework

This Unity3D package implements [Vectrosity] support for [Grid Framework]. It
features a new rendering backend and extension methods to grid renderers.

![Screenshot of sample scene with multiple grids rendered using a laser-line effect][screenshot]


## Installation

This Git repository is a regular standalone Unity3D package. You can read more
about it in the [documentation].


## Usage

In order to use this package you should be familiar with both Grid Framework
and Vectrosity.

There is a basic sample scene included under the `Samples/Basic` directory. To
use the Vectrosity rendering backed you need to add the `VectrosityBackend`
component to a game object which has a grid renderer component. Then set the
properties just as you would set them for a vector line and you should see your
grid in the scene.


## License

Released under the MIT license. Please see the [LICENSE] file for details.


[Vectrosity]: https://starscenesoftware.com/vectrosity.html
[Grid Framework]: http://hiphish.github.io/grid-framework/
[LICENSE]: LICENSE.md
[documentation]: Documentation~/com.hiphish.grid-framework.vectrosity.md
[screenshot]: Documentation~/scene.png "sample scene"
