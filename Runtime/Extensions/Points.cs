﻿using UnityEngine;
using System.Collections.Generic;
using GridFramework.Rendering;
using GridRenderer = GridFramework.Renderers.GridRenderer;

namespace GridFramework.Extensions.Vectrosity {
	/// <summary>
	///   Extension methods for getting Vectrosity points.
	/// </summary>
	public static class Points {
		/// <summary>
		///   Get a list of renderer points ready for use with Vectrosity.
		/// </summary>
		/// <returns>
		///   List of all points for all three axes.
		/// </returns>
		/// <param name="renderer">
		///   The instance to extend.
		/// </param>
		/// <remarks>
		///   <para>
		///     The axes are sorted from <c>x</c> to <c>z</c>.
		///   </para>
		/// </remarks>
		public static List<Vector3> GetVectrosityPoints(this GridRenderer renderer) {
			var (segments, _) = renderer.GetLineSegments();
			var result = new List<Vector3>(segments.Length);

			foreach (var group in segments) {
				foreach (var segment in group) {
					result.Add(segment.From);
					result.Add(segment.To);
				}
			}
			
			return result;
		}

		/// <summary>
		///   Get a list of renderer points ready for use with Vectrosity separated by axis.
		/// </summary>
		/// <returns>
		///   List of all points for all three axes, separated by axis.
		/// </returns>
		/// <param name="renderer">
		///   The instance to extend.
		/// </param>
		/// <remarks>
		///   <para>
		///     The axes are sorted from <c>x</c> to <c>z</c>.
		///   </para>
		/// </remarks>
		public static List<List<Vector3>> GetVectrosityPointsSeparate(this GridRenderer renderer) {
			var result = new List<List<Vector3>>(3);
			var (segments, _) = renderer.GetLineSegments();

			foreach (var group in segments) {
				var points = new List<Vector3>(2 * group.Length);
				result.Add(points);
				foreach (var segment in group) {
					points.Add(segment.From);
					points.Add(segment.To);
				}
			}

			return result;
		}

		/// <summary>
		///   Get a list of renderer points ready for use with Vectrosity from one axis.
		/// </summary>
		/// <returns>
		///   List of all points for one of the three axes.
		/// </returns>
		/// <param name="renderer">
		///   The instance to extend.
		/// </param>
		/// <param name="index">
		///   Index of the axis: <c>x=0</c>, <c>y=1</c>, <c>z=2</c>.
		/// </param>
		/// <remarks>
		///   If the index is less than <c>0</c> or greater than <c>2</c> an error will be thrown.
		/// </remarks>
		public static List<Vector3> GetVectrosityPointsSeparate(this GridRenderer renderer, int index) {
			RendererLineSegments segments;

			try {
				(segments, _) = renderer.GetLineSegments();
			} catch (System.IndexOutOfRangeException) {
				var message = $"The index {index} must be between zero (inclusive) and three (exclusive).";
				throw new System.IndexOutOfRangeException(message);
			}

			var result = new List<Vector3>(2 * segments[index].Length);
			foreach (var segment in segments[index]) {
				result.Add(segment.From);
				result.Add(segment.To);
			}

			return result;
		}
	}
}
