using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GridFramework.Renderers;
using GridFramework.Extensions.Vectrosity;
using Vectrosity;

using RenderingSystem = GridFramework.Rendering.System;

// FIXME: Continuous update does not work if gizmos are enabled in the scene

// Ideas for the future:
//   - Line properties need to be adjustable at runtime; requires properties and custom inspector
//   - End caps would be nice to have as property
namespace GridFramework.Rendering.Vectrosity {
	/// <summary> Rendering backed which uses Vectrosity. </summary>
	[RequireComponent(typeof(GridRenderer))]
	[AddComponentMenu("Grid Framework/Rendering/Vectrosity Backend")]
	public class VectrosityBackend: MonoBehaviour {
		/// <summary> Material for the Vectrosity line renderer.  </summary>
		public Material _material;

		/// <summary> Texture for Vectrosity lines. </summary>
		public Texture _texture;

		/// <summary> Color of the Vectrosity lines. </summary>
		public Color _color = new Color(1f, 0f, 0f, 1f);

		/// <summary> Width of the Vectrosity lines. </summary>
		public float _width = 10f;

		/// <summary>
		///   Whether to continually poll the grid renderers for changes.
		/// </summary>
		/// <remarks>
		///   If set to <c>false</c> the lines will be computed only once when the object becomes
		///   active. Otherwise the renderers are polled on each fixed frame update. This value can
		///   also be changed during runtime.
		/// </remarks>
		// Requires a proper GUI inspector to expose this property.
		/* private bool ContinuousPoll { */
		/* 	get => _continuousPoll; */
		/* 	set { */
		/* 		if (value && !_continuousPoll) { */
		/* 			RenderingSystem.RendererRegistered   += OnRendererRegister; */
		/* 			RenderingSystem.RendererUnregistered += OnRendererUnregister; */
		/* 		} else if (!value && _continuousPoll) { */
		/* 			RenderingSystem.RendererRegistered   -= OnRendererRegister; */
		/* 			RenderingSystem.RendererUnregistered -= OnRendererUnregister; */
		/* 		} */
		/* 		_continuousPoll = value; */
		/* 	} */
		/* } */

		/// <summary>
		///   Whether to continually poll the grid renderers for changes.
		/// </summary>
		/// <remarks>
		///   If set to <c>false</c> the lines will be computed only once when the object becomes
		///   active. Otherwise the renderers are polled on each fixed frame update. This value
		///   cannot be changed during runtime.
		/// </remarks>
		public bool ContinuousPoll = false;

		private VectorLine line;
		private ICollection<GridRenderer> renderers;
		private readonly IDictionary<GridRenderer, List<Vector3>> rendererPoints =
			new Dictionary<GridRenderer, List<Vector3>>();

		private void Awake() {
    		renderers = new List<GridRenderer>();

    		foreach (var renderer in GetComponents<GridRenderer>()) {
    			renderers.Add(renderer);
    			rendererPoints.Add(renderer, new List<Vector3>());
    		}

			RenderingSystem.RendererRegistered   += OnRendererRegister;
			RenderingSystem.RendererUnregistered += OnRendererUnregister;
		}

    	private void OnDestroy() {
			RenderingSystem.RendererRegistered   -= OnRendererRegister;
			RenderingSystem.RendererUnregistered -= OnRendererUnregister;
    	}

    	void OnEnable() {
    		foreach (var renderer in GetComponents<GridRenderer>()) {
    			rendererPoints[renderer] = renderer.GetVectrosityPoints();
    		}

			var name = "GridFramework_Vectrosity_" + this.ToString();
			var vectorPoints = CombinePoints(rendererPoints.Values);
			line = new VectorLine(name, vectorPoints, _texture, _width);
			line.color = _color;
			line.material = _material;
			line.drawTransform = transform;
			line.Draw3DAuto();

			RenderingSystem.PollRenderers(renderers);
			line.Draw();

    		if (ContinuousPoll) {
				RenderingSystem.RendererRefreshed += OnRendererRefresh;
    		}
    	}

    	void OnDisable() {
    		if (ContinuousPoll) {
				RenderingSystem.RendererRefreshed -= OnRendererRefresh;
    		}

    		VectorLine.Destroy(ref line);
    		line = null;
    	}

		private void FixedUpdate() {
			if (!ContinuousPoll) {
				return;
			}
			RenderingSystem.PollRenderers(renderers);
		}

		private void OnRendererRegister(GridRenderer renderer) {
    		if (!GetComponents<GridRenderer>().Contains(renderer) || renderers.Contains(renderer)) {
    			return;  // Avoid registering a renderer twice
    		}

			renderers.Add(renderer);
    		rendererPoints.Add(renderer, new List<Vector3>());
    		if (ContinuousPoll) {
				var (segments, refreshed) = RenderingSystem.PollRenderer(renderer);
				UpdateRendererPoints(renderer, segments);
    			UpdateVectorLine(line, CombinePoints(rendererPoints.Values));
    		}
		}

		private void OnRendererUnregister(GridRenderer renderer) {
    		if (!GetComponents<GridRenderer>().Contains(renderer) || !renderers.Contains(renderer)) {
    			return;
    		}
			renderers.Remove(renderer);
    		rendererPoints.Remove(renderer);
    		if (ContinuousPoll) {
    			UpdateVectorLine(line, CombinePoints(rendererPoints.Values));
    		}
		}

		private void OnRendererRefresh(GridRenderer renderer, RenderingSystem.RendererRefreshEventArgs args) {
    		if (!renderers.Contains(renderer)) {
    			return;
    		}
    		UpdateRendererPoints(renderer, args.Segments);
    		UpdateVectorLine(line, CombinePoints(rendererPoints.Values));
		}

		private void UpdateVectorLine(VectorLine line, List<Vector3> points) {
			if (line != null) {  // Race condition: the line might be destroyed before the renderers
    			line.points3 = points;
			}
		}

		private void UpdateRendererPoints(GridRenderer renderer, RendererLineSegments segments) {
    		var rendererPoints = this.rendererPoints[renderer];
    		rendererPoints.Clear();

    		foreach (var segmentGroup in segments) {
    			foreach (var segment in segmentGroup) {
    				foreach (var point in segment) {
    					rendererPoints.Add(point);
    				}
    			}
    		}
		}

		private List<Vector3> CombinePoints(ICollection<List<Vector3>> pointses) {
			var m = transform.worldToLocalMatrix;
			var p = transform.position;
			int size = pointses.Aggregate(0, (sum, points) => sum + points.Count);

			var result = new List<Vector3>(size);
			foreach (var points in pointses) {
				foreach (var point in points) {
					result.Add(m * (point - p));
				}
			}
			return result;
		}
	}
}
